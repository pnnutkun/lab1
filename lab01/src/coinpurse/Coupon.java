package coinpurse;

import java.util.Hashtable;
import java.util.Map;

/**
 * A coupon with a monetary value and colour.
 * You can't change the value of a coupon.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-27
 */
public class Coupon implements Valuable,Comparable<Valuable>{
	/** Value of Coupon. */
	private double value;
	/** Colour of Coupon. */
	private String color_of_Coupon;
	/** Colour map of Coupon. */
	static Map<String,Double> colour;
	
	static {
		colour = new Hashtable<String,Double>();
		colour.put("red", new Double(100));
		colour.put("blue", new Double(50));
		colour.put("green", new Double(20));
	}
	
	
	/**
	 * Constructor for a new coupon. 
	 * @param color of Coupon 
	 */
	public Coupon(String color)
	{
		color.toLowerCase();
		this.value = colour.get(color);
		color_of_Coupon = color;

		
	}
	
	@Override
	public double getValue()
	{
		return this.value;
	}
	
	/**
	 * @param  true if obj is a Coupon and has the same color as this Coupon.
	 * @return true when value and color of coupon be the same, false when not be the same. 
	 */
	public boolean equals( Object obj)
	{
		if ( obj == null )
		{
			return false;
		}
		if ( obj.getClass() == this.getClass() )
		{
			return false;
		}
		
		Coupon other = (Coupon)obj;
		if ( other.value == this.value && other.color_of_Coupon.equals(this.color_of_Coupon))
		{
			return true;
		}
		return false;
	}
	
	/**
     * Compare Coupon.
     * @param obj is the thing that you want to compare.
     * @return 1) -1 when value less than 2) 0 when same value 3) 1 when value greater than other 4) when null return Coupon must not be null
     */
	 public int compareTo(Valuable obj)
	    {
	    	Coupon other = (Coupon)obj;
	    	if(this.getValue() < other.getValue())
	    	{
	    		return -1;
	    	}
	    	else if(this.getValue() > other.getValue())
	    	{
	    		return 1;
	    	}
	    	else if(this.getValue() == other.getValue())
	    	{
	    		return 0;
	    	}
	    	else if(obj == null)
	    	{
	    		throw new NullPointerException("Coupon must not be null");
	    	}
	    	return -1;
	    }
	 
	/**
	 * @return describing the Coupon.
	 */
	public String toString()
	{
		return color_of_Coupon+" coupon";
	}
	
}
