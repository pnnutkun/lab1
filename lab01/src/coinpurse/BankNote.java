package coinpurse;

/**
 * A BankNote with a monetary value.
 * You can't change the value of a BankNote.
 * @author Pipatpol Tanavongchinda
 * @version 2015-01-27
 */
public class BankNote implements Valuable,Comparable<Valuable>{
	/** Value of Coupon */
	private double value;
	/** Start serial number of the BankNote */
	static private  int serialnumber = 1000000;
	/** Serial number of the BankNote */
	private int serial;
	
	/**
	 * Constructor for a new BankNote. 
	 * @param value of BankNote
	 */
	public BankNote(int value)
	{
		this.value = value;
		serial = serialnumber;
		serialnumber++;
	}
	
	/**
	 * @param true if obj is a BankNote and has the same value as this.
	 */
	public boolean equals( Object obj)
	{
		if ( obj == null )
		{
			return false;
		}
		if ( obj.getClass() == this.getClass() )
		{
			return false;
		}
		
		BankNote other = (BankNote)obj;
		if ( other.value == this.value )
		{
			return true;
		}
		return false;
	}
	
	/**
	 * @return the value of this BankNote.
	 */
	public double getValue()
	{
		return this.value;
	}
	 /**
     * Compare BankNote.
     * @param obj is the thing that you want to compare.
     * @return 1) -1 when value less than 2) 0 when same value 3) 1 when value greater than other 4) when null return BankNote must not be null
     */
	public int compareTo(Valuable obj)
    {
    	BankNote other = (BankNote)obj;
    	if(this.getValue() < other.getValue())
    	{
    		return -1;
    	}
    	else if(this.getValue() > other.getValue())
    	{
    		return 1;
    	}
    	else if(this.getValue() == other.getValue())
    	{
    		return 0;
    	}
    	else if(obj == null)
    	{
    		throw new NullPointerException("BankNote must not be null");
    	}
    	return -1;
    }
	 /**
     * @return Show the "xxx-Baht Banknote [serialnum]".
     */
	public String toString()
	{
		return this.value+"-Baht BankNote ["+this.serial+"]";
	}
}
